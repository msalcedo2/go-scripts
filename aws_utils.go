package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

type JobType int32

const (
	JobType_WWW              JobType = 0
	JobType_SCREENSHOT       JobType = 1
	JobType_ECHO             JobType = 2
	JobType_DNS_A            JobType = 3
	JobType_DNS_MXLOOKUP     JobType = 4
	JobType_DNS_NSLOOKUP     JobType = 5
	JobType_DNS_SOA          JobType = 6
	JobType_DNS_CONSOLIDATED JobType = 7
)

type Job struct {
	ID         string  `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Type       JobType `protobuf:"varint,2,opt,name=Type,proto3,enum=gatheringtypes.JobType" json:"Type,omitempty"`
	CreateTime int64   `protobuf:"varint,3,opt,name=createTime,proto3" json:"createTime,omitempty"`
	InputURL   string  `protobuf:"bytes,4,opt,name=inputURL,proto3" json:"inputURL,omitempty"`
	ResultURL  string  `protobuf:"bytes,5,opt,name=resultURL,proto3" json:"resultURL,omitempty"`
	Lane       uint32  `protobuf:"varint,6,opt,name=lane,proto3" json:"lane,omitempty"`
}

type JobState struct {
	JobID          string `json:"jobId,omitempty"`
	InputURL       string `json:"inputURL,omitempty"`
	CreateTime     int64  `json:"createTime,omitempty"`
	Type           string `json:"type,omitempty"`
	Status         string `json:"status,omitempty"`
	LastUpdateTime int64  `json:"lastUpdateTime,omitempty"`
	ResultURL      string `json:"resultURL,omitempty"`
	Lane           uint32 `json:"lane"`
}

func SaveJobState(ctx context.Context, dbClient *dynamodb.Client, tableName string, jobState JobState) error {
	_, err := dbClient.PutItem(ctx, &dynamodb.PutItemInput{
		TableName: aws.String(tableName),
		Item: map[string]types.AttributeValue{
			"jobId":          &types.AttributeValueMemberS{Value: jobState.JobID},
			"InputURL":       &types.AttributeValueMemberS{Value: jobState.InputURL},
			"CreateTime":     &types.AttributeValueMemberN{Value: fmt.Sprintf("%d", jobState.CreateTime)},
			"Type":           &types.AttributeValueMemberS{Value: jobState.Type},
			"Status":         &types.AttributeValueMemberS{Value: jobState.Status},
			"LastUpdateTime": &types.AttributeValueMemberN{Value: fmt.Sprintf("%d", jobState.LastUpdateTime)},
			"ResultURL":      &types.AttributeValueMemberS{Value: jobState.ResultURL},
			"Lane":           &types.AttributeValueMemberN{Value: fmt.Sprintf("%d", jobState.Lane)},
		},
	})
	if err != nil {
		return fmt.Errorf("failed to put item in DynamoDB, %w", err)
	}

	fmt.Println("JobState added Successfully")
	return nil
}

func cleanTable(ctx context.Context, dbClient *dynamodb.Client, tableName string) error {
	// First, scan the table to get all the primary key values
	scanOutput, err := dbClient.Scan(ctx, &dynamodb.ScanInput{
		TableName: aws.String(tableName),
		AttributesToGet: []string{
			"jobId",
		},
	})
	if err != nil {
		return fmt.Errorf("failed to scan table, %w", err)
	}

	// Then, delete each item based on its primary key
	for _, item := range scanOutput.Items {
		_, err := dbClient.DeleteItem(ctx, &dynamodb.DeleteItemInput{
			TableName: aws.String(tableName),
			Key:       item,
		})
		if err != nil {
			return fmt.Errorf("failed to delete item, %w", err)
		}
	}

	fmt.Println("Successfully cleaned table")
	return nil
}

func SendJobToSQS(ctx context.Context, sqsClient *sqs.Client, job *Job, queueURL string) (string, error) {
	serializedMessageBody, err := json.Marshal(job)
	if err != nil {
		log.Printf("error marshalling job to JSON: %v", err)
		return "", err
	}

	output, err := sqsClient.SendMessage(ctx, &sqs.SendMessageInput{
		QueueUrl:    aws.String(queueURL),
		MessageBody: aws.String(string(serializedMessageBody)),
	})
	if err != nil {
		log.Printf("failed to send message to SQS: %v", err)
		return "", err
	}

	log.Printf("Message sent to SQS with ID: %s", *output.MessageId)
	return *output.MessageId, nil
}

func CleanQueue(ctx context.Context, sqsClient *sqs.Client, queueURL string) error {
	_, err := sqsClient.PurgeQueue(ctx, &sqs.PurgeQueueInput{
		QueueUrl: aws.String(queueURL),
	})
	if err != nil {
		return err
	}
	fmt.Println("Queue has been purged successfully.")
	return nil
}

func run() {
	fmt.Println("Running...")
	aws_host := "http://localhost:4566"
	cfg, err := config.LoadDefaultConfig(context.TODO(),
		config.WithRegion("us-west-2"),
		config.WithEndpointResolverWithOptions(aws.EndpointResolverWithOptionsFunc(
			func(service, region string, options ...interface{}) (aws.Endpoint, error) {
				switch service {
				case dynamodb.ServiceID:
					return aws.Endpoint{
						URL:               aws_host,
						HostnameImmutable: true,
					}, nil
				case sqs.ServiceID:
					return aws.Endpoint{
						URL:               aws_host,
						HostnameImmutable: true,
					}, nil
				}
				// Fall back to default resolver
				return aws.Endpoint{}, &aws.EndpointNotFoundError{}
			})),
	)
	if err != nil {
		log.Fatalf("Unable to load SDK config, %v", err)
	}

	dbClient := dynamodb.NewFromConfig(cfg)

	tableName := "gathering-jobs"
	err = cleanTable(context.TODO(), dbClient, tableName)
	if err != nil {
		log.Fatalf("Error cleaning table: %v", err)
	}

	jobId := "job_test_1"
	timeJob := time.Now().Unix()
	jobState := JobState{
		JobID:          jobId,
		Type:           "0",
		CreateTime:     timeJob,
		LastUpdateTime: timeJob,
		InputURL:       fmt.Sprintf("%s/my-localstack-bucket/url_short.csv", aws_host),
		ResultURL:      fmt.Sprintf("%s/my-localstack-bucket/url_short_result.csv", aws_host),
		Lane:           0,
		Status:         "QUEUED",
	}
	err = SaveJobState(context.TODO(), dbClient, tableName, jobState)
	if err != nil {
		log.Fatalf("Error saving item: %v", err)
	}

	sqsClient := sqs.NewFromConfig(cfg)
	queueName := "gathering-screenshot-lane0"
	sqsInput := &sqs.GetQueueUrlInput{
		QueueName: aws.String(queueName),
	}
	getQueueURLResult, err := sqsClient.GetQueueUrl(context.TODO(), sqsInput)
	if err != nil {
		log.Fatalf("failed to get queue URL, %v", err)
	}
	err = CleanQueue(context.TODO(), sqsClient, *getQueueURLResult.QueueUrl)
	if err != nil {
		log.Fatalf("Unable to purge SQS queue, %v", err)
	}

	job := Job{
		ID:         jobId,
		Type:       JobType_WWW,
		CreateTime: timeJob,
		InputURL:   fmt.Sprintf("%s/my-localstack-bucket/url_short.csv", aws_host),
		ResultURL:  fmt.Sprintf("%s/my-localstack-bucket/url_short_result.csv", aws_host),
		Lane:       0,
	}
	_, err = SendJobToSQS(context.TODO(), sqsClient, &job, *getQueueURLResult.QueueUrl)
	if err != nil {
		log.Fatalf("Unable to send job to SQS, %v", err)
	}
}
